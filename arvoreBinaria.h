typedef struct TipoArvore{
    char num;
    struct TipoArvore* direita;
    struct TipoArvore* esquerda;
} TipoArvore;

TipoArvore* CriaArvoreBinariaVazia(char num);

TipoArvore* LiberaArvoreBinaria(TipoArvore* arvore);

TipoArvore* CriaArvoreBinaria(char item, TipoArvore* esquerda, TipoArvore* direita);

int TestaArvoreBinariaVazia(TipoArvore* arvore);

float AvaliaArvorePosFixa(TipoArvore* arvore);

int VerificaFolha(TipoArvore* arvore);

void InsereTipoArvore(TipoArvore** arvore, char num);

void PercorreArvoreBinariaPreOrdem(TipoArvore* arvore);

void PercorreArvoreBinariaPosOrdem(TipoArvore* arvore);

void ImprimeArvoreBinaria(TipoArvore *arvore, int l);
