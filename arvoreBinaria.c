#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"
#include "arvoreBinaria.h"

TipoArvore* CriaArvoreBinariaVazia(char num){
    TipoArvore* arvore;
    *(&arvore) = (TipoArvore*)malloc(sizeof(TipoArvore));
    (*(&arvore))->esquerda = NULL;
    (*(&arvore))->direita = NULL;
    (*(&arvore))->num = num;
    return arvore;
}

TipoArvore* CriaArvoreBinaria(char num, TipoArvore* esquerda, TipoArvore* direita){
    TipoArvore* arvore;
    *(&arvore) = (TipoArvore*) malloc(sizeof(TipoArvore));
    (*(&arvore))->num = num;
    (*(&arvore))->esquerda = esquerda;
    (*(&arvore))->direita = direita;
    return arvore;
}

int TestaArvoreBinariaVazia(TipoArvore* arvore){
    return arvore == NULL;
}

int VerificaFolha(TipoArvore* arvore){
    if (TestaArvoreBinariaVazia(arvore->esquerda) && TestaArvoreBinariaVazia(arvore->direita)){
        return 1;
    }else{
        return 0;
    }
}

float AvaliaArvorePosFixa(TipoArvore* arvore){
    float operando1 = 0, operando2 = 0, resultado = 0;
    if(!TestaArvoreBinariaVazia(arvore))
	{
		if (VerificaFolha(arvore)){
			return atoi(&arvore->num);
		}else{
			operando1 = AvaliaArvorePosFixa(arvore->esquerda);
		    operando2 = AvaliaArvorePosFixa(arvore->direita);

		    switch (arvore->num){
		    	case '+':
		    			resultado = operando2 + operando1;
		    		break;

		    	case '-' :
		    			resultado = operando1 - operando2;
		    		break;

		    	case '*' :
		    			resultado = operando2 * operando1;
		    		break;

		    	case '/' :
		    			resultado = operando1 / operando2;
		    		break;

		    	default : break;
    		}
    	}
    }
    return resultado;
}

void PercorreArvoreBinariaPreOrdem(TipoArvore* arvore){
    if(!TestaArvoreBinariaVazia(arvore)){
        printf("%c ", arvore->num);
        PercorreArvoreBinariaPreOrdem(arvore->esquerda);
        PercorreArvoreBinariaPreOrdem(arvore->direita);
    }
}

void PercorreArvoreBinariaPosOrdem(TipoArvore* arvore){
    if(!TestaArvoreBinariaVazia(arvore)){
        PercorreArvoreBinariaPosOrdem(arvore->esquerda);
        PercorreArvoreBinariaPosOrdem(arvore->direita);
        printf("%c ", arvore->num);
    }
}

void InsereTipoArvore(TipoArvore** arvore, char num){
    if(*arvore == NULL){
        *arvore = (TipoArvore*)malloc(sizeof(TipoArvore));
        (*arvore)->esquerda = NULL;
        (*arvore)->direita = NULL;
        (*arvore)->num = num;
    }else{
        if(num <= (*arvore)->num){
            InsereTipoArvore(&(*arvore)->esquerda, num);
        }
        if(num > (*arvore)->num){
            InsereTipoArvore(&(*arvore)->direita, num);
        }
    }
}

TipoArvore* LiberaArvoreBinaria(TipoArvore* arvore){
    if(!TestaArvoreBinariaVazia(arvore)){
        LiberaArvoreBinaria(arvore->esquerda);
        LiberaArvoreBinaria(arvore->direita);
        free(arvore);
    }
    return NULL;
}

void ImprimeArvoreBinaria(TipoArvore* arvore, int l)
{
    int i;
    if(!TestaArvoreBinariaVazia(arvore)){
        ImprimeArvoreBinaria(arvore->esquerda, l + 1);
        for (i = 0; i < l; i++)
            printf("   ");
            printf("  |");
        printf("%c\n\n", arvore->num);
        ImprimeArvoreBinaria(arvore->direita, l + 1);
    }
}
