typedef struct celula{
    TipoArvore* elem;
    struct celula *prox2;
} TipoCelulaArvore;

typedef struct{
    TipoCelulaArvore *topo;
} TipoPilhaArvore;

void CriaPilhaArvore(TipoPilhaArvore *pilha);

int TestaPilhaArvoreVazia(TipoPilhaArvore *pilha);

void InserePilhaArvore(TipoPilhaArvore *pilha, TipoArvore* elem);

int RemovePilhaArvore(TipoPilhaArvore *pilha);

TipoArvore* TopoPilhaArvore(TipoPilhaArvore *pilha);
