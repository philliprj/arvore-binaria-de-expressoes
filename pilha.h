typedef struct no{
    char elem;
    struct no *prox;
} TipoCelula;

typedef struct{
    TipoCelula *topo;
} TipoPilha;

void CriaPilha(TipoPilha *pilha);

int TestaPilhaVazia(TipoPilha *pilha);

void InserePilha(TipoPilha *pilha, char elem);

int RemovePilha(TipoPilha *pilha);

void LiberaPilha (TipoPilha *pilha);

char TopoPilha(TipoPilha *pilha);
