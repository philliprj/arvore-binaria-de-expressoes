#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"

void CriaPilha(TipoPilha *pilha){
    pilha->topo = NULL;
}

int TestaPilhaVazia(TipoPilha *pilha){
    if (pilha->topo==NULL)
        return 1;
    else
        return 0;
}

void InserePilha(TipoPilha *pilha, char elem){
    TipoCelula *celula;  
    celula = (TipoCelula *) malloc( sizeof(TipoCelula)); 
    celula->elem = elem;
    celula->prox = pilha->topo;
    pilha->topo = celula;
}

int RemovePilha(TipoPilha *pilha){
    TipoCelula *aux;
    
    if (!TestaPilhaVazia(pilha)){
        aux = pilha->topo;
        pilha->topo = pilha->topo->prox;
        free(aux);
        return 1;
    } else {
        puts("Pilha TestaPilhaVazia!");
        return 0;
    }
}

char TopoPilha(TipoPilha *pilha){
    return pilha->topo->elem;
}   
