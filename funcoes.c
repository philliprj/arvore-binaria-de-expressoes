#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"
#include "funcoes.h"
#include "arvoreBinaria.h"
#include "pilhaArvore.h"

void RecebeExpressao() {
    char expressao[256];

    printf("Entre com a expressão na forma infixa: ");

    setbuf(stdin, NULL);
    fgets(expressao, 256, stdin);
    printf("\n\n========================================================================\n");
    TrataExpressao(expressao);

    printf("\n\n");
}

void TrataExpressao(char *expressao){

    TipoArvore* arvoreAux;
    TipoArvore* arvoreAux2;

    int i = 0;
    char topoAux;

    TipoPilhaArvore pilhaOperando;
    TipoPilha pilhaOperador;
    TipoPilha pilhaAux;

    CriaPilhaArvore(&pilhaOperando);
    CriaPilha(&pilhaOperador);
    CriaPilha(&pilhaAux);

    /* Avalia expressão e insere na pilha de operando */

    while ((strcmp(&expressao[i], "") > 0)) {

        if ((strcmp(&expressao[i], " ") > 0) && (strcmp(&expressao[i], "+") > 0) && (strcmp(&expressao[i], "-") > 0) &&
            (strcmp(&expressao[i], "*") > 0) && (strncmp(&expressao[i], "/", 1) > 0) && (strcmp(&expressao[i], "(") > 0) &&
            (strcmp(&expressao[i], ")") > 0)){
            InserePilhaArvore(&pilhaOperando, CriaArvoreBinariaVazia(expressao[i]));

	        if(!TestaPilhaVazia(&pilhaOperador)){

		        switch (TopoPilha(&pilhaOperador)){

		            	case('*') : arvoreAux = pilhaOperando.topo->elem;
					                RemovePilhaArvore(&pilhaOperando);
					                arvoreAux2 = CriaArvoreBinaria(pilhaOperador.topo->elem, pilhaOperando.topo->elem, arvoreAux);
					                RemovePilhaArvore(&pilhaOperando);
					                RemovePilha(&pilhaOperador);
					                InserePilhaArvore(&pilhaOperando, arvoreAux2);
					                break;

					    case('/') : arvoreAux = pilhaOperando.topo->elem;
					                RemovePilhaArvore(&pilhaOperando);
					                arvoreAux2 = CriaArvoreBinaria(pilhaOperador.topo->elem, pilhaOperando.topo->elem, arvoreAux);
					                RemovePilhaArvore(&pilhaOperando);
					                RemovePilha(&pilhaOperador);
					                InserePilhaArvore(&pilhaOperando, arvoreAux2);
					                break;

					    default: break;

		        }
		    } /* Avalia expressão e insere na pilha de operadores */
    	}else if ((strncmp(&expressao[i], " ", 1) > 0)){

           	if ((strncmp(&expressao[i], ")", 1) > 0) || (strncmp(&expressao[i], "(", 1) == 0)) {
           		InserePilha(&pilhaOperador, expressao[i]);
           	}else{

           		topoAux = TopoPilha(&pilhaOperador);

           		if ((strncmp(&expressao[i], ")", 1) > 0) || (strncmp(&topoAux, "(", 1) == 0)){
           			RemovePilha(&pilhaOperador);
           		}else{

					arvoreAux = pilhaOperando.topo->elem;
					RemovePilhaArvore(&pilhaOperando);
					arvoreAux2 = CriaArvoreBinaria(pilhaOperador.topo->elem, pilhaOperando.topo->elem, arvoreAux);
					RemovePilhaArvore(&pilhaOperando);
					RemovePilha(&pilhaOperador);
					RemovePilha(&pilhaOperador);
					InserePilhaArvore(&pilhaOperando, arvoreAux2);
				}
           	}

        }

        i++;
    }

    while (!TestaPilhaVazia(&pilhaOperador)){
    	arvoreAux = pilhaOperando.topo->elem;
        RemovePilhaArvore(&pilhaOperando);
        arvoreAux2 = CriaArvoreBinaria(pilhaOperador.topo->elem, pilhaOperando.topo->elem, arvoreAux);
        RemovePilhaArvore(&pilhaOperando);
        RemovePilha(&pilhaOperador);
        InserePilhaArvore(&pilhaOperando, arvoreAux2);
    }

    printf("\n\nExpressão Infixa: %s", expressao);

    printf("\n\nÁrvore \n\n");
    ImprimeArvoreBinaria(pilhaOperando.topo->elem,0);
	printf("\n\nExpressão Posfixa: ");
	PercorreArvoreBinariaPosOrdem(pilhaOperando.topo->elem);
	printf("\n\nResultado Avaliação Posfixa: %.2f", AvaliaArvorePosFixa(pilhaOperando.topo->elem));
	printf("\n\n========================================================================\n");

    LiberaArvoreBinaria(arvoreAux);
}
