#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvoreBinaria.h"
#include "pilhaArvore.h"

void CriaPilhaArvore(TipoPilhaArvore *pilha){
    pilha->topo = NULL;
}

int TestaPilhaArvoreVazia(TipoPilhaArvore *pilha){
    if (pilha->topo==NULL)
        return 1;
    else
        return 0;
}

void InserePilhaArvore(TipoPilhaArvore *pilha, TipoArvore* elem){
    TipoCelulaArvore *celula;
    celula = (TipoCelulaArvore *) malloc( sizeof(TipoCelulaArvore));
    celula->elem = elem;
    celula->prox2 = pilha->topo;
    pilha->topo = celula;
}

int RemovePilhaArvore(TipoPilhaArvore *pilha){
    TipoCelulaArvore *aux;

    if (!TestaPilhaArvoreVazia(pilha)){
        aux = pilha->topo;
        pilha->topo = pilha->topo->prox2;
        free(aux);
        return 1;
    } else {
        puts("Pilha TestaPilhaVazia!");
        return 0;
    }
}

TipoArvore* TopoPilhaArvore(TipoPilhaArvore *pilha){
    return pilha->topo->elem;
}
